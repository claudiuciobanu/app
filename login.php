<!DOCTYPE html>
<html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style><!--
        #container{width:400px; margin: 0 auto;}
--></style>
<script type="text/javascript" language="javascript">
    function submitlogin()
    {
        var form = document.login;
        if(form.username.value == "")
        {
            alert( "Enter username." );
            return false;
        }else if(form.password.value == "")
        {
            alert( "Enter password." );
            return false;
        }
    }
</script>
<span style="font-family: 'Courier 10 Pitch', Courier, monospace; font-size: 13px; font-style: normal; line-height: 1.5;"><div id="container"></span>
<h1>Login Here</h1>
<form action="" method="post" name="login">
<table>
    <tbody>
        <tr>
            <th>Username:</th>
                <td><input type="text" name="username" required="" /></td>
       </tr>
        <tr>
            <th>Password:</th>
                <td><input type="password" name="password" required="" /></td>
       </tr>
            <td><input onclick="return(submitlogin());" type="submit" name="submit" value="Login" /></td>
            <td><a href="registration.php">Register new user</a></td>
    </tbody>
</table>
</form></div>

</html>
<?php
include_once 'user.php';
include_once 'functions.php';
    $user = new User();
    $update = new Crud();
    if (isset($_REQUEST['submit']))
    {
        extract($_REQUEST);
        $userData = $user->check_login($username, $password);
        if($userData != null  && $userData['status'] == '1')
        {
            if ($userData['type'] == 'student')
            {            
                header("location:student.php");
            } elseif ($userData['type'] == 'admin')
            {                        
                header("location:admin.php");
            } elseif ($userData['type'] == 'profesor')
            {     
                header("location:profesor.php");
            } else
            {     
                echo 'Wrong username or password';
            }
        }
        
    }
?>