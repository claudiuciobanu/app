<table id = "mytable" class = "table table-bordred table-striped">
<head><link rel="stylesheet" type ="text/css" href ="style.css"></head>
<thead>
  <th>Username</th> 
  <th>Edit</th>
  <th>Delete</th>
  <th>Status</th>

</thead>
<tbody>

<?php
require_once("functions.php");
$fetchdata = new Crud();
$users     = $fetchdata->fetchData();

foreach($users as $key => $data)
{
  if($data['status'] != 1)
  {
    $data['status'] = 'inactive';
  }
  else
  {
    $data['status'] = 'active';
  }
?>
      <tr>
        <td><?php echo htmlentities($data['username']);?></td>
        <td><a href ="update.php?id=<?php echo htmlentities($data['id']);?>"><button class = "btn btn-primary btn-xs"><span class =         "glyphicon glyphicon-pencil"></span></button></a></td>
        <td><a href ="delete.php?del=<?php echo htmlentities($data['id']);?>"><button class = "btn btn-danger btn-xs" onClick="return 
        confirm('Do you really want to delete');"><span class="glyphicon glyphicon-trash"></span></button></a></td>
        <td><?php echo htmlentities($data['status']);?></td>
      </tr>
<?php
}
      
?>
        
</tbody>
</table>
