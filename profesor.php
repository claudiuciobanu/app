<?php
include_once('db_config.php');
include_once('functions.php');
include_once('fetchSubcat.php');


if(isset($_POST['submit']))
{
    $file          = $_FILES['file'];
    //var_dump($file);
    $fileName      = $_FILES['file']['name'];
    $fileTmpName   = $_FILES['file']['tmp_name'];
    $fileSize      = $_FILES['file']['size'];
    $fileError     = $_FILES['file']['error'];
    $fileType      = $_FILES['file']['type'];
    $fileExt       = explode('.', $fileName);
    $fileActualExt = strtolower(end($fileExt));
    $allowed       = array('jpg', 'jpeg', 'png', 'pdf');
    $insertData    = new Crud();
    $sql        = $insertData->insert_files($fileName);
    if(in_array($fileActualExt, $allowed))
    {
        if($fileError === 0)
        {
            if($fileSize < 1000000)
            {
                $fileNameNew     = uniqid('', true).".".$fileActualExt;
                $fileDestination = 'uploads/'.$fileNameNew;
                move_uploaded_file($fileTmpName, $fileDestination);
                header("Location: profesor.php?uploadsuccess");
            }
            else
            {
                echo "Your file is too big!";   
            }
        }else
        {
            echo "There was an error uploading your file!";
        }
    }else
    {
        echo "You cannot upload files of this type!";
    }
}
?>
 <!DOCTYPE html>
<html>
<body>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"/>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>
<body>   
    <form action = "profesor.php" method = "POST" enctype = "multipart/form-data">
        <input type  = "file" name = "file">
    <button type = "submit" name = "submit">UPLOAD</button>
    </form>
    <br><br>
    <div class   = "col-md-4">
        <select name = "categoryList" id="categoryList" class = "form-control">
            <option value = "">Select category</option>
            <?php
            $fetchcategory = new Crud();
            $sql           = $fetchcategory->fetchCategory();
            foreach($sql as $key => $data)
            {
                echo '<option value="'.$data["id_category"].'">'.$data["category_name"].'</option>';
            }
            echo "</select>";
            ?>
        </select>
    </div>
    <div class   = "col-md-4">
        <button type = "button" name = "search" id = "search" class = "btn btn-info"> Search</button>
    </div>
    <br />
    <div class = "table-responsive" id = "categoryDetails" style = "display:none">
        <table class = "table table-bordered">
            <tr>
                <td width = "10%" align = "left"><b>Subcategories</b></td>
                <td width = "90%"><span id = "subcategoryName"></span></td>
            </tr>
            <tr>
                <td width = "10%" align = "left"><b>Subjects</b></td>
                <td width = "90%"><span id = "subjectName"></span></td>
            </tr>
    </div>
    
</body>
</html>

<script>
    $(document).ready(function()
    {
        $('#categoryList').click(function()
        {
            console.log($('#categoryList'));
            var id   = $('#categoryList').val();
            console.log(id);
            if(id != '')
            {
                $.ajax
                ({
                    url:"fetchSubcat.php",
                    method:"POST",
                    data:{id:id},
                    //dataType:'json',
                    success:function(data)
                    {
                        var results = JSON.parse(data);
                        $.each(results, function (key, value)
                        {
                            $('#categoryDetails').css("display", "block");
                            $('#subcategoryName').append(value.subcategory_name + '<br>');
                            $('#subjectName').append(value.subject_name + '<br>');
                        })  
                        $(document).ready(function()
                        {
                            $("#categoryList").click(function()
                            {
                            $("#subcategoryName").empty();
                            $("#subjectName").empty();
                            });
                        });
                         if(results.length == 0)
                         {
                            $('#categoryDetails').html('No subcategories were found!');
                         }
                        
                    }
                })
            }
            else
            {
                alert("Please select category");
                $('#categoryDetails').css("display","none");
            }
        });   
    })
        
</script>
